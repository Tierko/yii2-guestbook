<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\models\Post */

$this->title = 'Гостевая книга';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Оставить отзыв', ['create', 'maxId' => $maxId], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="messages">
        <div class="panel panel-default">
            <?php foreach ($posts as $post): ?>
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span><?= Html::encode("{$post->username}") ?></span>
                        <span class="pull-right label label-info"><?= Html::encode("{$post->created}") ?></span>
                    </h3>
                </div>
                <div class="panel-body">
                    <?= Html::encode("{$post->comment}") ?>
                    <hr>
                <?php if(!Yii::$app->user->isGuest): ?>
                    <div class="pull-right">
                        <?= Html::a('Редактировать', ['update', 'id' => $post->id], ['class' => 'btn btn-success']) ?>
                        <?= Html::a('Удалить', ['delete', 'id' => $post->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                    <? endif; ?>
                </div>
            <? endforeach; ?>
        </div>
        <?= LinkPager::widget(['pagination' => $pagination]) ?>
    </div>
</div>
