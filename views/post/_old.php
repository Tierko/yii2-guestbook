<?php
/**
 * Created by PhpStorm.
 * User: Константин
 * Date: 11.04.2016
 * Time: 3:39
 */

use yii\helpers\Html;
use yii\widgets\LinkPager;

?>


<div class="container">
    <form method="post" id="id-form_messages">
        <div class="form-group">
            <label for="name">Имя: </label>
            <input type="text" class="form-control" placeholder="Имя" name="name" id="name">
        </div>
        <div class="form-group">
            <label for="message">Сообщениие: </label>
            <textarea class="form-control" rows="5" placeholder="Текст" name="message" cols="50" id="message"></textarea>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Добавить">
        </div>
    </form>
    <hr>
    <div class="messages">
        <div class="panel panel-default">
            <?php foreach ($posts as $post): ?>
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span><?= Html::encode("{$post->username}") ?></span>
                        <span class="pull-right label label-info"><?= Html::encode("{$post->created}") ?></span>
                    </h3>
                </div>
                <div class="panel-body">
                    <?= Html::encode("{$post->comment}") ?>
                    <hr>
                    <div class="pull-right">
                        <a href="#" class="btn btn-info">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        <button class="btn btn-danger">
                            <i class="glyphicon glyphicon-trash"></i>
                        </button>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
        <?= LinkPager::widget(['pagination' => $pagination]) ?>
    </div>
