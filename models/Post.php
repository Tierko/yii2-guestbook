<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $username
 * @property string $comment
 * @property string $created
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'username', 'comment', 'created'], 'required'],
            [['id'], 'integer'],
            [['created'], 'safe'],
            [['username'], 'string', 'max' => 50],
            [['comment'], 'string', 'max' => 200],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Имя',
            'comment' => 'Отзыв',
            'created' => 'Время',
        ];
    }
}
